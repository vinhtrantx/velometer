import React from 'react';
import {
  View,
  Text,
  Button,
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';

class LocationBox extends React.Component{
	constructor(props){
		super(props);
		this.state={speed:0,heading:"N"}
		this.timeout = this.timeout.bind(this);
		Geolocation.watchPosition(this.timeout);
	}
	timeout(position){
		this.setState({speed:this.state.speed + 10});
	}
	render(){
		return (
		<View>
		<View><Text>Current Speed</Text>
			<Text>{this.state.speed}</Text></View>
		<View><Text>Direction</Text>
			<Text>{this.state.heading}</Text></View>
		</View>
		);
	}
}
export default LocationBox;